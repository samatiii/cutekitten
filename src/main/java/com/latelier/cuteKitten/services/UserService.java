package com.latelier.cuteKitten.services;

import com.latelier.cuteKitten.exceptions.AccountAlreadyExist;
import com.latelier.cuteKitten.model.Account;
import com.latelier.cuteKitten.model.User;

import java.util.Optional;

public interface UserService {

    /**
     * methode to finde or create the user
     * @param userId
     * @return
     */
    User findUser(Long userId );

    Optional<User> findByUsername(String username);

    void registerUser(User user) throws AccountAlreadyExist;
}
