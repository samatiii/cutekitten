package com.latelier.cuteKitten.services.impl;

import com.latelier.cuteKitten.model.Kitten;
import com.latelier.cuteKitten.repositories.KittenRepository;
import com.latelier.cuteKitten.services.KittenService;
import com.latelier.cuteKitten.utils.PreConditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KittenServiceImpl implements KittenService {

    @Autowired
    private KittenRepository kittenRepository;

    @Override
    public Kitten findOrCreatKitten(Kitten kitten) {
        PreConditions.checkNotNull(kitten,"Kitten Must be not null");
        Kitten kittenResult = kittenRepository.findById(kitten.getId()).orElse(null);
        if(kittenResult == null){
           return kittenRepository.save(kitten);
        }
        return kittenResult;
    }


}
