package com.latelier.cuteKitten.controllers;

import com.latelier.cuteKitten.dto.*;
import com.latelier.cuteKitten.model.Account;
import com.latelier.cuteKitten.model.User;
import com.latelier.cuteKitten.security.JwtTokenProvider;
import com.latelier.cuteKitten.services.UserService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;



    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequestDto loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);

        return ResponseEntity.ok(new JwtAuthenticationResponseDto(jwt));
    }

    @PostMapping("/signup")
    @ResponseBody
   public ResponseEntity<User> signUp(@Valid @RequestBody SignUpRequestDto signUpRequestDto) {

        User user = User
                .builder()
                .account(Account.builder().username(signUpRequestDto.getUsername())
                        .password(signUpRequestDto.getPassword()).build())
                .firstName(signUpRequestDto.getFirstname())
                .lastName(signUpRequestDto.getLastname())
                .build();

        userService.registerUser(user);

        return new ResponseEntity<User>(user, HttpStatus.OK);
    }
}
