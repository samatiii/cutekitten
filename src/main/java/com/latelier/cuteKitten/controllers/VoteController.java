package com.latelier.cuteKitten.controllers;

import com.latelier.cuteKitten.dto.VoteDto;
import com.latelier.cuteKitten.model.Kitten;
import com.latelier.cuteKitten.model.User;
import com.latelier.cuteKitten.model.Vote;
import com.latelier.cuteKitten.services.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/votes")
public class VoteController {


    @Autowired
    private VoteService voteService;

    @Autowired
    private UserDetailsService userDetailsService;

    @PostMapping
    public Vote vote(@RequestBody VoteDto voteDto, @AuthenticationPrincipal User user){

           return voteService.vote(user.getId(), new Kitten(voteDto.getKittenId(),voteDto.getKittenUrl()));
    }

    @GetMapping
    Page<Vote> getVotes(Pageable pageable){
        return voteService.getKittenVotes(pageable);
    }
}
