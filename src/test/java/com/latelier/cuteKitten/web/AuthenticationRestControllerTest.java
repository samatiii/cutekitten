package com.latelier.cuteKitten.web;

import com.latelier.cuteKitten.utils.AbstractRestControllerTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class AuthenticationRestControllerTest extends AbstractRestControllerTest {

    @BeforeEach
    public void setUp() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void whensigneUp_validBody_thenRetuenSatus200() throws Exception{
        getMockMvc().perform(post("/users/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"firstName\": \"lora\",\"lastName\": \"lora\",\"username\": \"lora\", \"password\": \"loralora\"}"))
                .andExpect(status().isOk());
    }

    @Test
    void whenSignUp_usernameInvalid_thenReturnsStatus400() throws Exception {

        getMockMvc().perform(post("/users/signup")
                .contentType("application/json")
                .content("{\"firstName\": \"lora\",\"lastName\": \"lora\",\"username\": \"\", \"password\": \"loralora\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenSignUp_InvalidPassword_thenReturnsStatus400() throws Exception {

        getMockMvc().perform(post("/users/signup")
                .contentType("application/json")
                .content("{\"firstName\": \"lora\",\"lastName\": \"lora\",\"username\": \"lora\", \"password\": \"\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenSignIn_getValidToken_thenRetuenStatus200() throws Exception {

        getMockMvc().perform(post("/users/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\": \"abdell\", \"password\": \"abdell\"}"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("accessToken")));
    }
    @Test
    public void whensigneIn_validBody_thenRetuenSatus200() throws Exception {
        getMockMvc().perform(post("/users/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\": \"abdell\", \"password\": \"abdell\"}"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("accessToken")));
    }

    @Test
    public void whensigneIn_WrongPassword_thenRetuenSatus401() throws Exception {
        getMockMvc().perform(post("/users/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\": \"abdell\", \"password\": \"wrong\"}"))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string(not(containsString("accessToken"))));
    }

    @Test
    public void whensigneIn_WrongUserName_thenRetuenSatus401() throws Exception {
        getMockMvc().perform(post("/users/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\": \"not_existing\", \"password\": \"password\"}"))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string(not(containsString("accessToken"))));
    }
}

