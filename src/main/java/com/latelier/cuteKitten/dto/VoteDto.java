package com.latelier.cuteKitten.dto;

import lombok.Data;

@Data
public class VoteDto {
    private String kittenId;
    private String kittenUrl;
}
