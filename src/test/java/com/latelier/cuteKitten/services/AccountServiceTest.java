package com.latelier.cuteKitten.services;

import com.latelier.cuteKitten.model.Account;
import com.latelier.cuteKitten.repositories.AccountRepository;
import com.latelier.cuteKitten.services.impl.AccountServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.mockito.Mockito.when;


public class AccountServiceTest {

    @InjectMocks
    private AccountServiceImpl acountService;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createAccountTest_success(){
        //given
        Account account = Account.builder().id(2323L).username("abdell").password("abdell").build();
        when(bCryptPasswordEncoder.encode("userPassword")).thenReturn("123Aer123");
        when(accountRepository.existsByUsername("abdell")).thenReturn(false);
        when(accountRepository.save(account)).thenReturn(account);

        //when
        Account result =  acountService.createAccount(account);

        //then
        Assertions.assertThat(result).isEqualTo(account);
    }

}
