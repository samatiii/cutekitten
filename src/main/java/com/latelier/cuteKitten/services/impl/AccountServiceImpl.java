package com.latelier.cuteKitten.services.impl;

import com.latelier.cuteKitten.exceptions.AccountAlreadyExist;
import com.latelier.cuteKitten.model.Account;
import com.latelier.cuteKitten.repositories.AccountRepository;
import com.latelier.cuteKitten.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Account createAccount(Account account) throws AccountAlreadyExist {

        if(accountRepository.existsByUsername(account.getUsername())) {
            throw new AccountAlreadyExist(account);
        }

        account.setPassword(bCryptPasswordEncoder.encode(account.getPassword()));

        return accountRepository.save(account);
    }
}
