package com.latelier.cuteKitten.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


@Data
public class SignUpRequestDto {

    @NotEmpty(message = "Please provide a firstname")
    @Size(min = 4, max = 40)
    private String firstname;

    @NotEmpty(message = "Please provide a lastName")
    @Size(min = 4, max = 40)
    private String lastname;

    @NotEmpty(message = "Please provide a username")
    @Size(min = 4, max = 15,message = "Username size must be longer than 4 characters")
    private String username;


    @NotEmpty(message = "Please provide a password")
    @Size(min = 6, max = 20, message = "pssword size must be longer than 6 characters")
    private String password;
}
