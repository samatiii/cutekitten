package com.latelier.cuteKitten.services;

import com.latelier.cuteKitten.model.Kitten;

public interface KittenService {

    /**
     * Methode to creat kitten
     * @param kitten
     * @return
     */
    Kitten findOrCreatKitten(Kitten kitten);
}
