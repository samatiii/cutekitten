package com.latelier.cuteKitten;

import com.latelier.cuteKitten.model.Account;
import com.latelier.cuteKitten.model.User;
import com.latelier.cuteKitten.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class CuteKittenApplication implements CommandLineRunner {

    @Autowired
    private UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(CuteKittenApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        User user = User.builder()

                .firstName("adbesamad")
                .lastName("fethllah")
                .account((Account.builder()
                        .username("abdell")
                        .password("abdell")
                        .build()))

                .build();


        userService.registerUser(user);
        System.out.println("user created with id : " + user.getId());
    }
}
