package com.latelier.cuteKitten.repositories;

import com.latelier.cuteKitten.model.Vote;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoteRepository extends JpaRepository<Vote,Long> {

    boolean existsByKittenId(String kittenId);

    Vote findByKittenId(String kittenId);

    Page<Vote> findByOrderByVoteCount(Pageable pageable);
}
