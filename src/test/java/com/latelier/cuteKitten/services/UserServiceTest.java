package com.latelier.cuteKitten.services;

import com.latelier.cuteKitten.model.Account;
import com.latelier.cuteKitten.model.User;
import com.latelier.cuteKitten.repositories.UserRepository;
import com.latelier.cuteKitten.services.impl.UserServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;

public class UserServiceTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private AccountService acountService;

    private Account account;
    private User user;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
        account = Account.builder().id(1234L).username("abdell").password("abdell").build();
        user = User.builder().firstName("abdell").lastName("abdell").account(account).createdAt(new Date()).id(2323L).build();
    }

    @Test
    public void whenFindUser_validId_thenOK(){
        when(userRepository.getOne(2323L)).thenReturn(user);
        User result = userService.findUser(2323L);
        Assertions.assertThat(result.getId()).isEqualTo(2323L);

    }

    @Test
    public void whenFindUser_validUserName_thenOK(){
        when(userRepository.findByAccountUsername("abdell")).thenReturn(Optional.of(user));
        User result = userService.findByUsername("abdell").get();
        Assertions.assertThat(result.getAccount().getUsername()).isEqualTo("abdell");

    }

    @Test
    public void whenFindUser_nonNalidUserName_thenOK(){
        when(userRepository.findByAccountUsername("abdell")).thenReturn(Optional.of(user));
        User result = userService.findByUsername("abdell").get();
        Assertions.assertThat(result.getAccount().getUsername()).isNotEqualTo("nonValidUsername");

    }






}
