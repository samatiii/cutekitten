package com.latelier.cuteKitten.web;

import com.latelier.cuteKitten.utils.AbstractRestControllerTest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static com.latelier.cuteKitten.utils.LogInUtils.getTokenForLogin;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class VoteControllerTest extends AbstractRestControllerTest {


    @Test
    public void whenGetVotes_authenticatedUser_thenReturn_status200() throws Exception {
        final String token = getTokenForLogin("abdell", "abdell", getMockMvc());

        getMockMvc().perform(get("/votes")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token))
                .andExpect(status().isOk());
    }

    @Test
    public void whenGetVotes_UnauthenticatedUser_thenReturn_status401() throws Exception {
        final String token = "";

        getMockMvc().perform(get("/votes")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token))
                .andExpect(status().isUnauthorized());
    }

}
