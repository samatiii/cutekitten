package com.latelier.cuteKitten.repositories;

import com.latelier.cuteKitten.model.Kitten;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KittenRepository extends JpaRepository<Kitten,String> {
}
