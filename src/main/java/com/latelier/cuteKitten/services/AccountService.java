package com.latelier.cuteKitten.services;

import com.latelier.cuteKitten.exceptions.AccountAlreadyExist;
import com.latelier.cuteKitten.model.Account;

public interface AccountService {

    Account createAccount(Account account) throws AccountAlreadyExist;
}
