package com.latelier.cuteKitten.services.impl;

import com.latelier.cuteKitten.exceptions.AccountAlreadyExist;
import com.latelier.cuteKitten.model.Account;
import com.latelier.cuteKitten.model.User;
import com.latelier.cuteKitten.repositories.UserRepository;
import com.latelier.cuteKitten.services.AccountService;
import com.latelier.cuteKitten.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AccountService acountService;

    @Override
    public User findUser(Long userId) {
        return userRepository.getOne(userId);
    }


    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByAccountUsername(username);
    }

    @Override
    public void registerUser(User user) throws AccountAlreadyExist {

        Account account = acountService.createAccount(user.getAccount());

        user.setAccount(account);

        userRepository.save(user);

    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(""));
    }
}
