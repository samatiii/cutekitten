package com.latelier.cuteKitten.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Table(name = "Vote")
public class Vote {
    @Id
    @GeneratedValue
    private Long id;

    @JsonIgnore
    @ManyToMany
    private Set<User> users = new HashSet<>();

    @OneToOne
    private Kitten kitten;

    private int voteCount = 0;

    public void addUser(User user) {
        if(users.add(user)) {
            this.voteCount++;
        }
    }
}
