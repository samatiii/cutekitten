package com.latelier.cuteKitten.utils;

public final class  PreConditions {

    public static void checkNotNull(Object o, String message){
        if(o == null){
            throw new IllegalArgumentException(message);
        }
    }
}
