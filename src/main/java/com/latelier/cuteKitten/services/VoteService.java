package com.latelier.cuteKitten.services;

import com.latelier.cuteKitten.exceptions.VoteAlreadyExistException;
import com.latelier.cuteKitten.exceptions.VoteNotFound;
import com.latelier.cuteKitten.model.Kitten;
import com.latelier.cuteKitten.model.Vote;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface VoteService {
    /**
     * methode to vote for a cat
     * @param userId
     * @param kitten
     * @return
     * @throws VoteAlreadyExistException
     */
    Vote vote(Long userId, Kitten kitten) throws VoteAlreadyExistException;

    /**
     * Methode to get kittenVotes
     * @param id
     * @return
     * @throws VoteNotFound
     */
    Page<Vote> getKittenVotes(Pageable pageable) throws VoteNotFound;
}
