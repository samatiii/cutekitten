package com.latelier.cuteKitten.services.impl;

import com.latelier.cuteKitten.exceptions.VoteAlreadyExistException;
import com.latelier.cuteKitten.exceptions.VoteNotFound;
import com.latelier.cuteKitten.model.Kitten;
import com.latelier.cuteKitten.model.User;
import com.latelier.cuteKitten.model.Vote;
import com.latelier.cuteKitten.repositories.VoteRepository;
import com.latelier.cuteKitten.services.KittenService;
import com.latelier.cuteKitten.services.UserService;
import com.latelier.cuteKitten.services.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class VoteServiceImpl implements VoteService {

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private KittenService kittenService;

    @Autowired
    private UserService userService;


    @Override
    public Vote vote(Long userId, Kitten kitten) throws VoteAlreadyExistException {

        Vote vote = null;

        final User foundUser = userService.findUser(userId);

        if (voteExists(kitten.getId())) {
            vote = voteRepository.findByKittenId(kitten.getId());
        } else {
            Kitten foundKitten = kittenService.findOrCreatKitten(kitten);
            vote = new Vote();
            vote.setKitten(foundKitten);
            voteRepository.save(vote);
        }

        vote.addUser(foundUser);

        return voteRepository.save(vote);
    }

    @Override
    public Page<Vote> getKittenVotes(Pageable pageable) throws VoteNotFound {
        Pageable paging = PageRequest.of(0,60,Sort.by("voteCount").descending());
        return voteRepository.findByOrderByVoteCount(paging);
    }


    private boolean voteExists(String kittenId) {
        return voteRepository.existsByKittenId(kittenId);
    }
}
