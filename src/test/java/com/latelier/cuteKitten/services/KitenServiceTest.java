package com.latelier.cuteKitten.services;

import com.latelier.cuteKitten.model.Kitten;
import com.latelier.cuteKitten.repositories.KittenRepository;
import com.latelier.cuteKitten.services.impl.KittenServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import java.util.Optional;


public class KitenServiceTest {

    @InjectMocks
    private KittenServiceImpl kittenService;

    @Mock
    private KittenRepository kittenRepository;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findKitten_withValidArgument_thenOK(){
        Kitten kitten = Kitten.builder().id("123AZER123").url("wwww.cutekitten.png").build();
        when(kittenRepository.findById("123AZER123")).thenReturn(Optional.of(kitten));

        Kitten result= kittenService.findOrCreatKitten(kitten);

        Assertions.assertThat(result.getId()).isEqualTo(kitten.getId());

    }

    @Test
    public void creatKitten_withInValidArgument_thenOK(){
        Kitten kitten = Kitten.builder().id("123AZER123").url("wwww.cutekitten.png").build();
        when(kittenRepository.findById(null)).thenReturn(null);
        when(kittenRepository.save(kitten)).thenReturn(kitten);

        Kitten result= kittenService.findOrCreatKitten(kitten);

        Assertions.assertThat(result).isEqualTo(kitten);

    }

    @Test
    public void throwException__when_InvalidpArgument() throws Exception {

        final String invalid = "Kitten Must be not null";

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            kittenService.findOrCreatKitten(null);
        });

        Assertions.assertThat(invalid).isEqualTo(exception.getMessage());

    }



}
